#![feature(test)]
extern crate test;
// tui configuration module
pub mod config;

fn main() -> Result<(), Box<dyn std::error::Error>>{
     // Tui.rs configuration function
     config::tui()
}