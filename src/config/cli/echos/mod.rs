use std::fs::File;

use clap::{Command, ArgMatches, Arg, parser::ValuesRef};

use serde_json::Value;

use state_box::{EchosState, StateBox, StateEnum};


pub fn command() -> Command<'static> {
    common_args(Command::new("echos")
        .about("ya echos subcommand"))
}

fn common_args(cmd: Command<'static>) -> Command<'static> {
    let json_schema: Value = serde_json::from_reader(File::open("./ya_log_state.schema.json").unwrap()).unwrap();
    let properties = json_schema.get("properties").unwrap().as_object().unwrap().to_owned().into_iter();

    cmd.args(
        properties.map(|e| {
            let k: &str = Box::leak(e.0.to_owned().into_boxed_str());
            let value: &str = Box::leak(e.1.as_str().unwrap_or_default().to_owned().into_boxed_str());
             Arg::new(k)
                .long(k) 
                .takes_value(true)
                .required(true)
                .default_value(value)
        })
    )
}


pub fn matches(matches: &ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
    match matches.subcommand_matches("echos") {
        Some(m) => {
            common(m)
        }, 
        None => {Ok(None)},
    }
}
// If there is many args to be made into a list we have to use a plural form for the argument.
fn common(m: &ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
    let arg = m.get_many("message");
    let msg:&String = match arg
    {
        Some(_msg) => match _msg.last() {
            Some(__msg) => __msg,
            _ =>  {return Ok(None)}
        },
        _ => {return Ok(None)} 
    };
    
    Ok(Some(msg.to_string()))
}

pub fn structy(m: &ArgMatches) -> Result<Option<StateBox>, Box<dyn std::error::Error>> {
    match m.subcommand_matches("echos") {
        Some(m) => {

            match common(m) {
                Ok(Some(msg)) => {                    
                    let result = StateBox {udomid:"ya_echos".to_string(), state: StateEnum::EchosState(EchosState {message: msg}) };
                    Ok(Some(result))
                },
                _ => Ok(None)
            }
            
        }, 
        None => {Ok(None)},
    }
}

pub fn state(m: &ArgMatches) -> Result<Option<Vec<Vec<String>>>, Box<dyn std::error::Error>> {
    match m.subcommand_matches("echos") {
        Some(arg) => {
            let path = "./echo_state.json";
            if File::open(path).is_ok() {
                let file = File::open(path).unwrap();
                let state: EchosState = serde_json::from_reader(file).unwrap();
            
                if arg.get_many::<String>("message").is_some() {
                    let msgs: ValuesRef<String> = arg.get_many("message").unwrap();
                    return Ok(Some(vec![
                        vec!["message".to_string(), format!("{}{}{}",'"',msgs.last().unwrap_or(&"".to_string()),'"'), format!("{}{}{}",'"',state.message,'"')], 
                        // Using format to ensure the string nature of the argument
                    ]))
                } else {
                    return Ok(Some(vec![
                        vec!["message".to_string(), "".to_string(), format!("{}{}{}",'"',state.message,'"')],
                    ]))
                }
            }
            return Ok(Some(vec![
                vec!["message".to_string(), "".to_string(), "".to_string()],
            ]))
        }, 
        None => {Ok(None)},
    }
}
