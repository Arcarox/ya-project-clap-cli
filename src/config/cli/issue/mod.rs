use clap::{Command, ArgMatches};

mod title;

pub fn command() -> Command<'static> {
    Command::new("issue")
        .about("ya gitlab issues subcommand")
        .subcommand_required(true)
        .subcommand(common_args(title::command())
        )
}

fn common_args(cmd: Command<'static>) -> Command<'static> {
    cmd
}

pub fn matches(matches: &ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
    match matches.subcommand_matches("issue") {
        Some(m) => {
            if let Some(args) = m.subcommand_matches("title") {
                   return title::matches(args);
            }
            // else if let Some(args)= m.subcommand_matches("enter") {
            //     return common(args,'>');
            // } else  if let Some(args)= m.subcommand_matches("exit") {
            //     return common(args,'<');
            // }
            return Ok(None) ;
        }, 
        None => {Ok(None)},
    }
}
