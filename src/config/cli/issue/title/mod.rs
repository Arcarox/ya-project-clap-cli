use serde::Deserialize;
use gitlab::Gitlab;
use gitlab::{api, api::{Query}};

use clap::{Command, ArgMatches, Arg};

pub fn command() -> Command<'static> {
    Command::new("title")
        .about("ya issue's title subcommand")
        .arg(Arg::new("uuid")
        .long("uuid") 
        .takes_value(true)
        .help("The UUID of the issue")
        .required(true))
}

// Uncomment if using a subcommand here
// fn common_args(cmd: Command<'static>) -> Command<'static> {
//     cmd
// }

pub fn matches(matches: &ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
        match matches.get_one("uuid") {
            Some(v) => {
                get_title(v)
            },
            None => {Ok(None)},
        }
}

fn get_title(m: &String) -> Result<Option<String>, Box<dyn std::error::Error>> {
    #[derive(Debug, Deserialize)]
    struct Issue {
        title: String,
    }

    // Create the client.
    let client = match Gitlab::new("gitlab.com", "glpat-2zseezFL99sk_NMpwWaQ") {
        Ok(c) => {c},
        Err(e) => {return Err(Box::new(e))},
    };

    let endpoint = match api::issues::ProjectIssues::builder().project("bashroom-devs/workspace-reference").search_in(api::issues::IssueSearchScope::Description).search(m.to_owned()).build() {
        Ok(x) => {x},
        Err(e) => {return Err(Box::new(e));},
    };
    
    let projects:  Vec<Issue> = match endpoint.query(&client) {
        Ok(x) => {x},
        Err(e) => {return Err(Box::new(e));},
    };

    let result: String;
    if projects.len() > 0 {
        result = projects[0].title.to_owned();
    } else {
        result = "Could not find an open issue with this UUID".to_string();
    }

    Ok(Some(result))
}