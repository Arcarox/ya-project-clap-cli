use gitlab::Gitlab;
use gitlab::{api, api::{Query}};

use clap::{Command, ArgMatches, Arg};

pub fn command() -> Command<'static> {
    Command::new("delete")
        .about("ya labels' delete subcommand")
        .arg(Arg::new("names")
        .long("names") 
        .takes_value(true)
        .help("The names of the labels to delete")
        .required(true)
        .multiple_values(true))
}

// Uncomment if using a subcommand here
// fn common_args(cmd: Command<'static>) -> Command<'static> {
//     cmd
// }

pub fn matches(matches: &ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
        match matches.get_many::<String>("names") {
            Some(v) => {
                delete_lables(v.collect())
            },
            None => {Ok(None)},
        }
}

fn delete_lables(v: Vec<&String>) -> Result<Option<String>, Box<dyn std::error::Error>> {
    // Create the client.
    let client = match Gitlab::new("gitlab.com", "glpat-2zseezFL99sk_NMpwWaQ") {
        Ok(c) => {c},
        Err(e) => {return Err(Box::new(e))},
    };

    for name in v {
        
        let endpoint = match api::projects::labels::DeleteLabel::builder().project("bashroom-devs/workspace-reference").label(name.as_str()).build() {
            Ok(x) => {x},
            Err(e) => {return Err(Box::new(e));},
        };

        match endpoint.query(&client) {
            Ok(x) => {x},
            Err(_) => {},
        };

    }

    Ok(Some("Labels deleted succefuly!".to_string()))
}