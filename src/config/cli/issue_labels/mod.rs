use clap::{Command, ArgMatches};

mod create;
mod delete;

pub fn command() -> Command<'static> {
    Command::new("issue-labels")
        .about("ya gitlab labels subcommand")
        .subcommand_required(true)
        .subcommand(common_args(create::command()))
        .subcommand(common_args(delete::command()))
}

fn common_args(cmd: Command<'static>) -> Command<'static> {
    cmd
}

pub fn matches(matches: &ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
    match matches.subcommand_matches("issue-labels") {
        Some(m) => {
            if let Some(args) = m.subcommand_matches("create") {
                   return create::matches(args);
            }
            else if let Some(args)= m.subcommand_matches("delete") {
                return delete::matches(args);
            }
            // } else  if let Some(args)= m.subcommand_matches("exit") {
            //     return common(args,'<');
            // }
            return Ok(None) ;
        }, 
        None => {Ok(None)},
    }
}
