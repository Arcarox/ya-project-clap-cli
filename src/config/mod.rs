use clap::Command;
use crossterm::{
    event::{self, Event as CEvent, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode},
};
use std::{io};
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, Instant};
use tui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Style, Modifier},
    widgets::{
        Block, BorderType, Borders, Paragraph, Table, Row, TableState,
    },
    Terminal,
};

use std::fs::File;
use std::io::prelude::*;

use jsonschema::JSONSchema;
use serde_json::{Value};
use std::{fs, path::Path};
use std::env;

use state_box::{StateBox};


// CLI module
pub mod cli;

// Event ennumeration for tui rendering
enum Event<I> {
    Input(I),
    Tick,
}

// Main tui-rs configuration function
pub fn tui() -> Result<(), Box<dyn std::error::Error>> {
    enable_raw_mode().unwrap();

    // writing_bool
    let mut writing_cmd = false;
    let mut writing_form = false;

    // command and result str
    let mut cmd = "".to_string();
    let mut res  = "".to_string();

    // CLI
    let cli = cli::get_cli();

    // Canonical args
    let mut incoming_widget: Vec<Vec<String>> = vec![];
    let mut args = update_args(&"".to_string(),&cli);

    // Form state
    let mut form_state = TableState::default();

    // history str
    let mut hist = "".to_string();
    let mut form_hist = incoming_widget.to_owned();

    let (tx, rx) = mpsc::channel();
    let tick_rate = Duration::from_millis(200);
    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));

            if event::poll(timeout).unwrap(){
                if let CEvent::Key(key) = event::read().unwrap() {
                    tx.send(Event::Input(key)).unwrap();
                }
            }

            if last_tick.elapsed() >= tick_rate {
                if let Ok(_) = tx.send(Event::Tick) {
                    last_tick = Instant::now();
                }
            }
        }
    });

    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.clear()?;

    loop {
        terminal.draw(|rect| {
            let size = rect.size();
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(2)
                .constraints(
                    [
                        Constraint::Percentage(10),
                        Constraint::Percentage(10),
                        Constraint::Percentage(45),
                        Constraint::Percentage(30),
                        Constraint::Length(3),
                        Constraint::Length(3),
                    ]
                    .as_ref(),
                )
                .split(size);
            

            let terminal_width = ((rect.size().width as f64)*0.92 )as u16;
            let command: Paragraph;
            if writing_cmd {
                let print_cmd = &mut cmd.to_string();
                (0..(cmd.len()/terminal_width as usize)).enumerate().for_each(|(_,i)| print_cmd.insert_str(( terminal_width as usize)*(i+1), "\n"));
                command = Paragraph::new(format!("> {}", print_cmd))
                .style(Style::default().fg(Color::White))
                .alignment(Alignment::Left)
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .style(Style::default().fg(Color::Blue))
                        .title("Wish")
                        .border_type(BorderType::Plain),
                );

            } else {
                let print_cmd = &mut cmd.to_string();
                (0..(cmd.len()/terminal_width as usize)).enumerate().for_each(|(_,i)| print_cmd.insert_str(( terminal_width as usize)*(i+1), "\n"));
                command = Paragraph::new(format!("> {}", print_cmd))
                .style(Style::default().fg(Color::White))
                .alignment(Alignment::Left)
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .style(Style::default().fg(Color::White))
                        .title("Wish")
                        .border_type(BorderType::Plain),
                );

            }

            let print_tcmd = &mut cfg_solver(&cmd).to_string();
                (0..(cmd.len()/terminal_width as usize)).enumerate().for_each(|(_,i)| print_tcmd.insert_str(( terminal_width as usize)*(i+1), "\n"));

            let mut true_command = Paragraph::new(format!("> {}", print_tcmd))
                    .style(Style::default().fg(Color::White))
                    .alignment(Alignment::Left)
                    .block(
                        Block::default()
                            .borders(Borders::ALL)
                            .style(Style::default().fg(Color::Red))
                            .title("Command")
                            .border_type(BorderType::Plain),
                );
            if args.is_some() {
                    true_command = Paragraph::new(format!("> {}", print_tcmd))
                        .style(Style::default().fg(Color::White))
                        .alignment(Alignment::Left)
                        .block(
                            Block::default()
                                .borders(Borders::ALL)
                                .style(Style::default().fg(Color::Green))
                                .title("Command")
                                .border_type(BorderType::Plain),
                    );
            }
            

            let form = Table::new(to_args_rows(&incoming_widget))
            .style(Style::default().fg(Color::White))
            .header(
                Row::new(vec!["Args", "Value", "_state Value"])
                    .style(Style::default().fg(Color::Yellow))
                    .bottom_margin(1)
            )
            .block(Block::default().title("Form").borders(Borders::all()))
            .widths(&[Constraint::Percentage(30), Constraint::Percentage(30), Constraint::Percentage(30)])
            .highlight_style(Style::default().add_modifier(Modifier::BOLD))
            .highlight_symbol(">>");


            let mut print_res = &mut "".to_string();
            print_res = res.to_string().lines().map(|l| {
                let mut ls = l.to_string();
                (0..(ls.to_string().len()/terminal_width as usize)).enumerate().for_each(|(_,i)| ls.insert_str(( terminal_width as usize)*(i+1), "\n"));
                ls
            }).fold(print_res, |i,l| {i.push_str(format!("{}\n",l).as_str()); i });
            let result = Paragraph::new(format!("{}",print_res))
                .style(Style::default().fg(Color::Green))
                .alignment(Alignment::Left)
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .style(Style::default().fg(Color::White))
                        .title("Result")
                        .border_type(BorderType::Plain),
                );
                
            let help = Paragraph::new("Press: 'q'->quit -- ';'->type argument -- ':'->type command -- 'ENTER'->enter input -- 'V'->Submit input -- 'ESC'->exit w mode -- 'h'-> help")
                .style(Style::default().fg(Color::LightMagenta))
                .alignment(Alignment::Left)
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .style(Style::default().fg(Color::White))
                        .title("Help")
                        .border_type(BorderType::Plain),
                );

            let copyright = Paragraph::new("\nBashroom.com - all rights reserved")
                .style(Style::default().fg(Color::LightCyan))
                .alignment(Alignment::Center)
                .block(
                    Block::default()
                        .borders(Borders::ALL)
                        .style(Style::default().fg(Color::White))
                        .title("Copyright")
                        .border_type(BorderType::Plain),
                );

            // rect.render_widget(intro, chunks[0]);
            rect.render_widget(command, chunks[0]);
            rect.render_widget(true_command, chunks[1]);
            rect.render_stateful_widget(form, chunks[2], &mut form_state);
            // rect.render_widget(result, form[1]);
            rect.render_widget(result, chunks[3]);
            rect.render_widget(help, chunks[4]);
            rect.render_widget(copyright, chunks[5]);
            
        })?;

        // Update args
        args = update_args(&cmd, &cli);
        let stateless_cmd = &cli.to_owned().get_matches_from(parse_enter(&cfg_solver(&cmd)));

        match rx.recv()? {
            Event::Input(event) => {
                if writing_cmd || writing_form {
                    match event.code {
                        KeyCode::Enter => {
                            if writing_cmd {
                                hist = cmd.to_string();
                                incoming_widget = cli::state(stateless_cmd);
                                incoming_widget = incoming_widget.to_owned().into_iter().map(|mut e| {
                                    if e[1].is_empty() {
                                        let env_var = match env::var(&e[0]) {
                                            Ok(var) => {
                                                var
                                            },
                                            Err(_) => "".to_string(),
                                        };
                                        if env_var.is_empty() {
                                            e[1] = e[2].to_owned();
                                        } else {
                                            e[1] = format!("{}{}{}",'"',env_var,'"');
                                        }
                                    }
                                    e
                                }).collect();
                                form_hist = incoming_widget.to_owned();
                                writing_cmd = false;
                                if incoming_widget.len() > 0 {
                                    writing_form = true;
                                    form_state.select(Some(0));
                                }
                            } else if writing_form {
                                incoming_widget = incoming_widget.to_owned().into_iter().map(|mut v| {
                                    v[2] = v[1].to_owned();
                                    v
                                }).collect();
                                form_state.select(None);
                                writing_form = false;
                            }
                        }
                        KeyCode::Char(c) => {
                            if writing_cmd {
                                cmd.push(c);
                            }
                            if writing_form {
                                incoming_widget[form_state.selected().unwrap()][1].push(c);
                            }
                        }
                        KeyCode::Backspace => {
                            if writing_cmd {
                                cmd.pop();
                            }
                            if writing_form {
                                incoming_widget[form_state.selected().unwrap()][1].pop();
                            }
                        }
                        KeyCode::Esc => {
                            if writing_cmd {
                                writing_cmd = false;
                            }
                            if writing_form {
                                incoming_widget = form_hist.to_owned();
                                form_state.select(None);
                                writing_form = false;
                            }
                        }
                        KeyCode::Up => {
                            if writing_cmd {
                                cmd = hist.to_string();
                            }
                            if writing_form {
                                if form_state.selected().unwrap() > 0 {
                                    form_state.select(Some(form_state.selected().unwrap() - 1));
                                } else {
                                    form_state.select(Some(incoming_widget.len()-1));
                                }
                            }
                        }
                        KeyCode::Down => {
                            if writing_form {
                                if form_state.selected().unwrap() < (incoming_widget.len()-1) {
                                    form_state.select(Some(form_state.selected().unwrap() + 1));
                                } else {
                                    form_state.select(Some(0));
                                }
                            }
                        }
                        _ => {}  
                    }
                } else
                {match event.code {
                    KeyCode::Char('V') => {
                        if args.is_some() || incoming_widget.len()>0 {
                            let ref_cli = &cli;
                            let stateful_cmd = format!("{} {}", cmd,vec_args_to_cmd(&incoming_widget));
                            let parse_res = cli::subcom_matche(&ref_cli.to_owned().get_matches_from(parse_enter(&cfg_solver(&stateful_cmd))));
                            match parse_res {
                                Ok(s) => {res = s},
                                Err(e) => {res = e.to_string()},
                            }
                        } else {
                            res = "Your command is not valid".to_string();
                        }
                    }
                    KeyCode::Char(';') => {
                        writing_form = incoming_widget.len() > 0;
                        form_state.select(Some(0));
                        form_hist = incoming_widget.to_owned();
                    }
                    KeyCode::Char(':') => {
                        cmd = "".to_string();
                        writing_cmd = true;
                    }
                    KeyCode::Char('h') => {
                        let ref_cli = &cli;
                        let mut out: Vec<u8> = Vec::new();
                        ref_cli.to_owned().write_help(&mut out)?;
                        res = String::from_utf8(out).unwrap();
                    }
                    KeyCode::Char('q') => {
                        disable_raw_mode()?;
                        terminal.clear()?;
                        terminal.show_cursor()?;
                        break;
                    }
                    _ => {}       
                }}
        },
            Event::Tick => {}
        }
    }

    Ok(())
}

// private treatment function of the writen command string
fn parse_enter<'a> (cmd: &'a String) -> Vec<&'a str> {
    let mut args:Vec<&str> = vec![];

    let mut str_split: Vec<&str> = cmd.split('"').collect();
    let mut length = str_split.len();
    if str_split[length-1] == "" {
        str_split.pop();
        length = length - 1;
    }
    for i in 0..length {
        if i%2 == 0 {
            args.append(&mut str_split[i].split_ascii_whitespace().collect());
        } else {
            args.push(str_split[i]);
        }
    }
    args.push(" ");
    args
}

// Solving for the cfgs path
fn cfg_solver(cmd: & String) -> String {
    if cmd.starts_with("--cfgPath") {
        return cmd.to_owned();
    }
    let result = cmd.split("--cfgPath").map(|s| {
        if !(s == cmd.split("--cfgPath").nth(0).unwrap() || s.is_empty()) {
            let path = s.split_ascii_whitespace().nth(0).unwrap_or("");
            let mut remain = s.replace(path, "");
            if !path.is_empty() && File::open(path).is_ok() {
                let mut file = File::open(path).unwrap();
                let mut contents = String::new();
                file.read_to_string(&mut contents).unwrap();
                contents.push_str(remain.as_str());
                remain = contents;
            }
            return remain;
        }
        s.to_string()
    }).reduce(|mut a,s| {a.push_str(s.as_str()); a }).unwrap();

    parse_enter(&result).into_iter().map(|s| {s.into()}).rev().reduce(|mut res: String, e|{
        if e.starts_with("-") {
            if res.contains(&*e) {
                if res.starts_with('"') {
                    res = res[1..].to_string();
                    format!("{}{}{}",'"',">",res)
                } else {
                    format!("{}{}",">",res)
                }
            } else {
                format!("{} {}",e,res)
            }
        } else {
            if res.contains(">") {
                res = parse_enter(&res).into_iter().map(|s| {
                    if s.starts_with(">") || s.starts_with(format!("{}>",'"').as_str()) {
                        "".into()
                    } else {
                        s.into()
                    }
                }).reduce(|c: String,s| {
                    if s.contains(' ') && s!=" " {
                        format!("{} {}{}{}",c,'"',s,'"')
                    } else {
                        format!("{} {}",c,s)
                    }
                }).unwrap().trim().into();
            }
            if e.contains(' ') {
                format!("{}{}{} {}",'"',e,'"',res)
            } else {
                format!("{} {}",e,res)
            }
        }
    }).unwrap_or_default()

}

// Transform canonical args Vec into Rows
fn to_args_rows<'a>(vec_rows: &'a Vec<Vec<String>>) -> Vec<Row<'a>>{
    vec_rows.iter().map(|v| {
        Row::new(v.iter().map(|s| {s.to_owned()})).style(Style::default().fg(Color::Blue)).bottom_margin(1)
    }).collect()
}

fn vec_args_to_cmd(vec_rows: &Vec<Vec<String>>) -> String {
    vec_rows.into_iter().map(|e| {
        format!("--{} {}", e[0], e[2])
    }).collect()
}

pub fn validate_info(instance: Value, path: String) -> bool {

    let f = match fs::read_to_string(Path::new(&path)) {
        Ok(s) => {s},
        Err(_) => {return false;},
    };
    let schema = serde_json::from_str(f.as_str()).unwrap();

    let compiled = match JSONSchema::compile(&schema)
     {
        Ok(v) => {v},
        Err(_) => {return false;},
    };

    let result = compiled.validate(&instance);

    if let Err(_) = result {
        return false;
    }
    true
}

fn update_args(cmd: &String, cli: &Command) -> Option<StateBox> {
    let stateless_cmd = &cli.to_owned().get_matches_from(parse_enter(&cfg_solver(cmd)));
    cli::structy(stateless_cmd)
}